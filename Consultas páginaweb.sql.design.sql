﻿-- Consultas sobre página Web. El proyecto de Alejandro.

-- Título de libro y nombre del autor.
-- producto cartesiano
  SELECT a.nombre_completo,l.titulo 
  FROM libro l,autor a
  WHERE l.autor= a.id_autor;

-- Misma consulta usando el Join.
  SELECT a.nombre_completo,l.titulo
  FROM libro l JOIN autor a
  ON l.autor= a.id_autor;

-- Misma consulta join como producto cartesiano (no usar).
  SELECT a.nombre_completo,l.titulo
  FROM libro l JOIN autor a
  WHERE l.autor= id_autor;

-- Sacar el nombre del autor y título en el que han ayudado.

  SELECT a1.nombre_completo,l.titulo 
    FROM libro l
    JOIN ayuda a ON l.id_libro = a.libro
    JOIN autor a1 ON a.autor = a1.id_autor;
  
-- La misma consulta con JOIN 

  SELECT a.nombre_completo, titulo
    FROM libro l JOIN autor a JOIN ayuda a1
    ON a.id_autor = a1.autor
    AND l.id_libro = a1.libro;

-- Los libros que se han descargado cada usuario con la fecha de descarga. Listar el login, fecha descarga y el id _libro.

  SELECT * FROM fechadescarga f;

-- Los libros que se han descargado cada usuario con la fecha de descarga. Listar el login, correo, fecha descarga, título del libro.

  SELECT f.libro, f.usuario, l.titulo, u.email, f.fecha_descarga  

  FROM fechadescarga f JOIN descarga d 
  ON f.libro = d.libro AND f.usuario = d.usuario

  JOIN usuario u1 ON d.usuario = u1.login
  JOIN libro l ON libro = l.id_libro;

-- Indicar el nº de libros que hay 

  SELECT COUNT(*) FROM libro l;
  
-- Indicar el nº de libros por colección

  SELECT l.coleccion, COUNT(*) FROM libro l
  GROUP BY l.coleccion;

-- Indicar la colección que tiene más libros.

--  Subconsulta c1

  SELECT l.coleccion, COUNT(*)  nlibros FROM libro l
  GROUP BY l.coleccion;

--  Subconsulta c2

  SELECT MAX(c1.nlibros) máximo FROM 
    (SELECT l.coleccion, COUNT(*) nlibros FROM libro l
  GROUP BY l.coleccion) c1;

-- Consulta final

  SELECT * FROM (SELECT l.coleccion, COUNT(*)  nlibros FROM libro l
  GROUP BY l.coleccion) c1 
  JOIN (SELECT MAX(c1.nlibros) máximo FROM 
    (SELECT l.coleccion, COUNT(*) nlibros FROM libro l
  GROUP BY l.coleccion) c1) c2
  ON c1.nlibros= c2.máximo;

--  La colección que tiene menos libros 

  SELECT * FROM (SELECT l.coleccion, COUNT(*)  nlibros FROM libro l
  GROUP BY l.coleccion) c1 
  JOIN (SELECT MIN(c1.nlibros) mínimo FROM 
    (SELECT l.coleccion, COUNT(*) nlibros FROM libro l
  GROUP BY l.coleccion) c1) c2
  ON c1.nlibros= c2.mínimo;

-- La misma consulta de máximos con where
SELECT * FROM (

SELECT l.coleccion, COUNT(*)  nlibros FROM libro l
GROUP BY l.coleccion ) c1
  WHERE nlibros= ( (SELECT MAX(c1.nlibros) máximo FROM 
    (SELECT l.coleccion, COUNT(*) nlibros FROM libro l
  GROUP BY l.coleccion) c1));

-- La misma con Having.
SELECT l.coleccion, COUNT(*)  nlibros FROM libro l
GROUP BY l.coleccion
  HAVING nlibros= (SELECT MAX(c1.nlibros) máximo FROM 
    (SELECT l.coleccion, COUNT(*) nlibros FROM libro l
  GROUP BY l.coleccion)c1);

-- La colección que tiene menos libros con where

  SELECT * FROM  (

  SELECT l.coleccion, COUNT(*) nlibros FROM libro l
  GROUP BY l.coleccion ) c1
  WHERE nlibros= (SELECT MIN(c1.nlibros) mínimo FROM 
    (SELECT l.coleccion, COUNT(*) nlibros FROM libro l
    GROUP BY l.coleccion)c1);

-- La colección que tiene menos libros con having

  SELECT l.coleccion, COUNT(*)  nlibros FROM libro l
  GROUP BY l.coleccion
  HAVING nlibros= (SELECT min(c1.nlibros) mínimo FROM 
    (SELECT l.coleccion, COUNT(*) nlibros FROM libro l
  GROUP BY l.coleccion)c1);

-- El nombre de la colección que tiene más libros.

  SELECT c.nombre FROM coleccion c
    JOIN (
    SELECT * FROM (SELECT l.coleccion, COUNT(*)  nlibros FROM libro l
  GROUP BY l.coleccion) c1 
  JOIN (SELECT MAX(c1.nlibros) máximo FROM -- El Join en este caso te permite unir una tabla con una subconsulta.
    (SELECT l.coleccion, COUNT(*) nlibros FROM libro l
  GROUP BY l.coleccion) c1) c2
  ON c1.nlibros= c2.máximo
    ) consulta7 -- Aquí se le pone el alias de lo que se ha puesto entre paréntesis.
   ON c.id_coleccion=consulta7.coleccion; -- Aquí se le indica por qué se conecta la tabla con la consulta 7.    
  
 -- El nombre de la colección que tiene menos libros.

  SELECT c.nombre FROM coleccion c
    JOIN (
    SELECT * FROM (SELECT l.coleccion, COUNT(*)  nlibros FROM libro l
  GROUP BY l.coleccion) c1 
  JOIN (SELECT MIN(c1.nlibros) mínimo FROM -- El Join en este caso te permite unir una tabla con una subconsulta.
    (SELECT l.coleccion, COUNT(*) nlibros FROM libro l
  GROUP BY l.coleccion) c1) c2
  ON c1.nlibros= c2.mínimo
    ) consulta8 -- Aquí se le pone el alias de lo que se ha puesto entre paréntesis.
   ON c.id_coleccion=consulta8.coleccion; -- Aquí se le indica por qué se conecta la tabla con la consulta 7.

-- El nombre del libro que se ha descargado más veces.

  -- c1 Es la consulta1 con el nº de descargas por libro.

  SELECT f.libro, count(*) ndescargas FROM fechadescarga f
  GROUP BY f.libro;

  -- c2 Es la consulta2 con el nº máximo de descargas por libro.

  SELECT MAX(c1.ndescargas) FROM (
  SELECT f.libro, count(*) ndescargas FROM fechadescarga f
  GROUP BY f.libro
    ) c1;

   -- c3 Es la consulta3. El código del libro que se ha descargado más veces.

  SELECT MAX(c2.ndescargas) FROM (
  SELECT f.libro, f.fecha_descarga, count(*) ndescargas FROM fechadescarga f
  GROUP BY f.fecha_descarga
    ) c2;

  
-- El nombre del libro que se ha descargado más veces. Final. 

  SELECT l.titulo FROM libro l
    JOIN (
    SELECT * FROM (SELECT l.titulo COUNT(*) ndescargas FROM fechadescarga f, libro l
  GROUP BY l.titulo) c1 
    JOIN (SELECT Max(c1.nlibros) máximo FROM -- El Join en este caso te permite unir una tabla con una subconsulta.
    (SELECT l.titulo, COUNT(*) nlibros FROM fechadescarga f
  GROUP BY l.titulo) c1) c2
  ON c1.nlibros= c2.máximo
         ) consulta -- Aquí se le pone el alias de lo que se ha puesto entre paréntesis.
  ON l.titulo=consulta.titulo;

-- El nombre del usuario que ha descargado más libros.

  --c1 Número de descargas por usuario.
  
  SELECT f.usuario, count(*)ndescargas
    FROM fechadescarga f
  GROUP BY f.usuario;
 --c2 Máximo de c1

  SELECT MAX(c1.descargas) maximo
    FROM (
    SELECT f.usuario, count(*)ndescargas
    FROM fechadescarga f
  GROUP BY f.usuario) c1;

 -- Final con join 
  
  SELECT c1.usuario FROM 
    (SELECT f.usuario, count(*) ndescargas
    FROM fechadescarga f
  GROUP BY f.usuario
    ) c1
    JOIN 
    (SELECT MAX(c1.ndescargas) maximo
    FROM (
    SELECT f.usuario, count(*) ndescargas
    FROM fechadescarga f
  GROUP BY f.usuario) c1
    ) c2
    ON c1.ndescargas=c2.maximo;

  -- final con having 
    
    SELECT usuario,COUNT(*) ndescargas
      FROM fechadescarga f GROUP BY f.usuario
      HAVING ndescargas=
      (
        SELECT MAX(c1.ndescargas) maximo FROM
          (
              SELECT f.usuario,COUNT(*) ndescargas
      FROM fechadescarga f 
      GROUP BY f.usuario
          ) c1
      );

-- El nombre de los usuarios que han descargado más libros que Adam3.

-- C1: nº de descargas de Adam3

SELECT COUNT(*) FROM fechadescarga f
  WHERE f.usuario='Adam3'; 

-- C2: descargas que ha realizado cada usuario

SELECT f.usuario, COUNT(*) nlibros FROM fechadescarga f
  GROUP BY f.usuario;

-- Consulta final

SELECT f.usuario, COUNT(*) nlibros FROM fechadescarga f
  GROUP BY f.usuario
  HAVING nlibros> (
  SELECT COUNT(*) FROM fechadescarga f
  WHERE f.usuario='Adam3'
                  );

-- Mes que más libros se han descargado.

-- C1: el nº de descargas por mes.

SELECT MONTH(f.fecha_descarga) mes, COUNT (*) ndescargas FROM fechadescarga f
  GROUP BY MONTH(f.fecha_descarga);

--C2: el nº máximo de descargas por mes.

SELECT MAX(c1.ndescargas) maximo FROM 
  ( SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas FROM fechadescarga f
  GROUP BY MONTH(f.fecha_descarga)
    ) c1;

-- total

  SELECT mes FROM 
(SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas FROM fechadescarga f
  GROUP BY MONTH(f.fecha_descarga)
    )c1
    JOIN
    (
SELECT MAX(c1.ndescargas) maximo FROM 
  ( SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas FROM fechadescarga f
  GROUP BY MONTH(f.fecha_descarga)
  ) c1
    )c2
    ON c1.ndescargas=c2.maximo;

  -- Final con having

SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas FROM fechadescarga f
  GROUP BY MONTH(f.fecha_descarga)
HAVING ndescargas=
(
SELECT MAX(c1.ndescargas) maximo FROM 
  ( SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas FROM fechadescarga f
  GROUP BY MONTH(f.fecha_descarga)
    ) c1
);
