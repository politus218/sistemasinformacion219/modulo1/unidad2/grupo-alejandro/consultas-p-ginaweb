﻿-- Consultas sobre página Web. El proyecto de Alejandro.

-- Título de libro y nombre del autor.
-- producto cartesiano
  SELECT a.nombre_completo,l.titulo 
  FROM libro l,autor a
  WHERE l.autor= a.id_autor;

-- Misma consulta usando el Join.
  SELECT a.nombre_completo,l.titulo
  FROM libro l JOIN autor a
  ON l.autor= a.id_autor;

-- Misma consulta join como producto cartesiano (no usar).
  SELECT a.nombre_completo,l.titulo
  FROM libro l JOIN autor a
  WHERE l.autor= id_autor;

-- Sacar el nombre del autor y título en el que han ayudado.

  SELECT a1.nombre_completo,l.titulo 
    FROM libro l
    JOIN ayuda a ON l.id_libro = a.libro
    JOIN autor a1 ON a.autor = a1.id_autor;
  
-- La misma consulta con JOIN 

  SELECT a.nombre_completo, titulo
    FROM libro l JOIN autor a JOIN ayuda a1
    ON a.id_autor = a1.autor
    AND l.id_libro = a1.libro;

-- Los libros que se han descargado cada usuario con la fecha de descarga. Listar el login, fecha descarga y el id _libro.

  SELECT * FROM fechadescarga f;

-- Los libros que se han descargado cada usuario con la fecha de descarga. Listar el login, correo, fecha descarga, título del libro.

  SELECT f.libro, f.usuario, l.titulo, u.email, f.fecha_descarga  

  FROM fechadescarga f JOIN descarga d 
  ON f.libro = d.libro AND f.usuario = d.usuario

  JOIN usuario u1 ON d.usuario = u1.login
  JOIN libro l ON libro = l.id_libro;